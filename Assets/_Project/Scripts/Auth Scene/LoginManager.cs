using System.Collections;
using _Project.Scripts.Core;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;

namespace _Project.Scripts.Auth_Scene
{
    public class LoginManager : MonoBehaviour
    {
        public TMP_Text messageText;
        public TMP_InputField emailField;
        public TMP_InputField passField;
        public ButtonControllerAuth buttonController;
        
        public void Login()
        {
            if (string.IsNullOrEmpty(emailField.text) || string.IsNullOrEmpty(passField.text))
            {
                return;
            }
            
            
            messageText.text = "";
            StartCoroutine(AuthenticateUser());
        }

        private IEnumerator AuthenticateUser()
        {
            using (var uwr = UnityWebRequest.Get(AllUrls.LoginUrl + "email=" + emailField.text + "&" + "password=" + passField.text))
            {
                Debug.Log(uwr.url);
                Debug.Log(emailField.text);
                Debug.Log(passField.text);

                yield return uwr.SendWebRequest();

                if (uwr.isNetworkError || uwr.isHttpError)
                {
#if UNITY_EDITOR
                    Debug.Log(uwr.error);
                    Debug.Log(uwr.downloadHandler.text);
#endif
                }

                else
                {
#if UNITY_EDITOR
                    Debug.Log(uwr.downloadHandler.text);
#endif
                    AppManager.Instance.userInfo = new UserInfoResponse();
                    AppManager.Instance.userInfo = JsonUtility.FromJson<UserInfoResponse>(uwr.downloadHandler.text);
                    
                    if (AppManager.Instance.userInfo.UserData.Count != 0)
                    {
                        messageText.text = "";
                        LoginSuccessful();
                    }
                    else
                    {
                        messageText.text = "Login Failed";
                        Debug.Log("login Failed");
                    }
                }
            }
        }

        private void LoginSuccessful()
        {
            Debug.Log("LoginSuccessful");
            SetRole();
            buttonController.LoadCustomizationScene();
        }

        private void SetRole()
        {
            if (AppManager.Instance.userInfo.UserData[0].role == "Moderator")
            {
                AppManager.Instance.currentUserRole = UserRole.Moderator;
            }
            else if (AppManager.Instance.userInfo.UserData[0].role == "Audience")
            {
                AppManager.Instance.currentUserRole = UserRole.Audience;
            }
            else if (AppManager.Instance.userInfo.UserData[0].role == "Speaker")
            {
                AppManager.Instance.currentUserRole = UserRole.Speaker;
            }
        }
    }
}
