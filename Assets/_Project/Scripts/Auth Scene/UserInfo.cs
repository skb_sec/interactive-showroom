using System;
using System.Collections.Generic;

namespace _Project.Scripts.Auth_Scene
{

    [Serializable]
    public class UserData
    {
        public int id;
        public string name;
        public string email;
        public string position;
        public string company;
        public string role;
        public int event_id;
        public string event_name;
    }

    [Serializable]
    public class UserInfoResponse
    {
        public bool success;
        public List<UserData> UserData;
    }
}
