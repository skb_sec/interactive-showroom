using System;
using UnityEngine;
using UnityEngine.Android;

namespace _Project.Scripts.Auth_Scene
{
    public class GetMicrophonePermission : MonoBehaviour
    {
        private void Start()
        {
#if UNITY_ANDROID
            if (!Permission.HasUserAuthorizedPermission(Permission.Microphone))
            {
                Permission.RequestUserPermission(Permission.Microphone);
            }
#endif
        }
    }
}
