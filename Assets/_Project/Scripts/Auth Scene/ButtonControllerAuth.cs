using _Project.Scripts.Core;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace _Project.Scripts.Auth_Scene
{
    public class ButtonControllerAuth : MonoBehaviour
    {
        public TMP_InputField nameField;
        
        public void LoadCustomizationScene()
        {
            if (AppManager.Instance.isMaleAvatar)
            {
                SceneManager.LoadScene("Male Customization");
                return;
            }
            SceneManager.LoadScene("Female Customization");
        }

        public void SetUserRoleSpeaker(bool isOn)
        {
            if (isOn)
            {
                SetUserRole(UserRole.Speaker);
            }
        }
        
        public void SetUserRoleModerator(bool isOn)
        {
            if (isOn)
            {
                SetUserRole(UserRole.Moderator);
            }
        }
        
        public void SetUserRoleAudience(bool isOn)
        {
            if (isOn)
            {
                SetUserRole(UserRole.Audience);
            }
        }


        private void SetUserRole(UserRole userRole)
        {
            AppManager.Instance.currentUserRole = userRole;
        }
    }
}
