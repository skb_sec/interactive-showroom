﻿using Doozy.Engine.UI;
using UnityEngine.Events;

namespace _Project.Scripts.Core
{
    public class PopupScript
    {
        public static UIPopup s_popup;
        static UnityAction s_okButtonAction;

        public static void ShowPopup(string popupName, string title, string message, UnityAction okButtonAction = null, string buttonLabel = "")
        {
            if (s_popup != null)
            {
                return;
            }

            s_okButtonAction = okButtonAction;
            
            s_popup = UIPopup.GetPopup(popupName);
            
            if (s_popup == null) return;

            if (buttonLabel != "")
            {
                s_popup.Data.Buttons[0].TextLabel.text = buttonLabel;
            }
            // Debug.Log(message);
            string[] labels = new[] { title, message};
            s_popup.Data.SetLabelsTexts(labels);

            s_popup.Data.SetButtonsCallbacks(OnOKButtonClick);
            
            s_popup.Show();
        }

        private static void OnOKButtonClick() 
        {
            ClosePopup();
            s_okButtonAction?.Invoke();
            s_okButtonAction = null;
        }
    
        public static void ClosePopup()
        {
            if (s_popup != null)
            {
                s_popup.Hide();
            }
        }
    }
}