﻿using UnityEngine;

namespace _Project.Scripts.Core
{
    [RequireComponent(typeof(Canvas))]
    public class AssignCamera : MonoBehaviour
    {
        private Camera _mCamera;

        private void Start()
        {
            _mCamera = Camera.main;
            GetComponent<Canvas>().worldCamera = _mCamera;
        }
    }
}
