﻿using UnityEngine;

namespace _Project.Scripts.Core
{
    public class DeleteAllChild : MonoBehaviour
    {
        public static void Clear(Transform parent)
        {
            foreach (Transform child in parent)
            {
                Destroy(child.gameObject);
            }
        }
    }
}
