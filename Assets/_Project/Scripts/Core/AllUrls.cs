﻿namespace _Project.Scripts.Core
{
    public static class AllUrls
    {
        private const string BaseUrl = "http://comultra.com/auditorium/";

        public static readonly string UserInfoAnalyticsUrl = BaseUrl + "UserInfoAnalytics";
        public static readonly string UserTalkedAnalyticsUrl = BaseUrl + "talk.php?";
        public static readonly string ChatLogAnalyticsUrl = BaseUrl + "chat.php?";
        public static readonly string LoginUrl = BaseUrl + "users.php?";
        
        public static readonly string AllPresentationUrl = BaseUrl + "presentations.php";
    }
}
