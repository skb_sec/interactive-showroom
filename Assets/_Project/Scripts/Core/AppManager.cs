﻿using _Project.Scripts.Auth_Scene;
using UnityEngine;

namespace _Project.Scripts.Core
{
    public class AppManager : MonoBehaviour
    {
        public UserInfoResponse userInfo;
        public const byte MAX_PLAYER_IN_PHOTON_ROOM = 100;
        public string  gameVersion;
        //public string userName;
        
        public bool isMaleAvatar = true;
        public UserRole currentUserRole = UserRole.Audience;

        public static AppManager Instance;
        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            DontDestroyOnLoad(gameObject);
        }
    }
}

public enum UserRole
{
    Speaker,
    Moderator,
    Audience
}
