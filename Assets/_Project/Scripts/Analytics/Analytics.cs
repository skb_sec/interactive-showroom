using System;

[Serializable]
public class UserInfoAnalytics
{
    public string uid;
    public string user;
    public string userType;
    public string loginTime;
}

[Serializable]
public class UserTalkedAnalytics
{
    public string uid;
    public string user;
    public bool talked;
}

[Serializable]
public class ChatLogAnalytics
{
    public string chatLog;
}