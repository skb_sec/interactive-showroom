using System.Collections;
using System.IO;
using System.Text;
using _Project.Scripts.Core;
using UnityEngine;
using UnityEngine.Networking;

namespace _Project.Scripts.Analytics
{
    public class AnalyticsManager : MonoBehaviour
    {
        public UserInfoAnalytics userInfoAnalytics;
        public UserTalkedAnalytics userTalkedAnalytics;
        public ChatLogAnalytics chatLogAnalytics;

//         #region LogIn
//
//         public void SendUserInfoAnalytics()
//         {
//             userInfoAnalytics.uid = AppManager.Instance.userInfo.userData.uid;
//             userInfoAnalytics.user = AppManager.Instance.userInfo.userData.name;
//             userInfoAnalytics.userType = AppManager.Instance.userInfo.userData.userType;
//             userInfoAnalytics.loginTime = System.DateTime.Now.ToString();
//             
//             var info = JsonUtility.ToJson(userInfoAnalytics);;
//             StartCoroutine(SendUserInfoAnalyticsWeb(info));
//         }
//         
//         private IEnumerator SendUserInfoAnalyticsWeb(string data)
//         {
//             using (UnityWebRequest uwr = UnityWebRequest.Post(AllUrls.UserInfoAnalyticsUrl, data))
//             {
//                 Debug.Log(data);
//                 
//                 byte[] bodyRaw = Encoding.UTF8.GetBytes(data);
//                 uwr.uploadHandler = new UploadHandlerRaw(bodyRaw);
//                 uwr.downloadHandler = new DownloadHandlerBuffer();
//                 uwr.SetRequestHeader("Content-Type", "application/json");
//  
//                 yield return uwr.SendWebRequest();
//
//                 if (uwr.isNetworkError || uwr.isHttpError)
//                 {
// #if UNITY_EDITOR
//                     Debug.Log(uwr.error);
//                     Debug.Log(uwr.downloadHandler.text);
// #endif
//                 }
//
//                 else
//                 {
// #if UNITY_EDITOR
//                     Debug.Log(uwr.downloadHandler.text);
// #endif
//                 }
//             }
//         }
//
//         #endregion


        #region Talked

        [ContextMenu("Talked")]
        public void SendUserTalkedAnalytics()
        {
            StartCoroutine(SendUserTalkedAnalyticsWeb());
        }
        
        
        private IEnumerator SendUserTalkedAnalyticsWeb()
        {
            using (UnityWebRequest uwr = UnityWebRequest.Get(AllUrls.UserTalkedAnalyticsUrl + "id=" + AppManager.Instance.userInfo.UserData[0].id))
            {
                Debug.Log(uwr.url);
                yield return uwr.SendWebRequest();

                if (uwr.isNetworkError || uwr.isHttpError)
                {
#if UNITY_EDITOR
                    Debug.Log(uwr.error);
                    Debug.Log(uwr.downloadHandler.text);
#endif
                }

                else
                {
#if UNITY_EDITOR
                    Debug.Log(uwr.downloadHandler.text);
#endif
                }
            }
        }

        #endregion


        #region ChatLog

        public void SendChatLogAnalytics(string chatText)
        {
            Debug.Log(chatText);
            StartCoroutine(SendChatLogAnalyticsWeb(chatText));
            StoreChatLogLocal(chatText);
        }
        
        
        private IEnumerator SendChatLogAnalyticsWeb(string chatLog)
        {
            using (UnityWebRequest uwr = UnityWebRequest.Get(AllUrls.ChatLogAnalyticsUrl + "log=" + chatLog))
            {
                Debug.Log(uwr.url);

                yield return uwr.SendWebRequest();

                if (uwr.isNetworkError || uwr.isHttpError)
                {
#if UNITY_EDITOR
                    Debug.Log(uwr.error);
                    Debug.Log(uwr.downloadHandler.text);
#endif
                }

                else
                {
#if UNITY_EDITOR
                    Debug.Log(uwr.downloadHandler.text);
#endif
                }
            }
        }

        public void StoreChatLogLocal(string chatText)
        {
            string chatFileName = "ChatLog" + AppManager.Instance.userInfo.UserData[0].event_name + ".txt";
            
            string path = Application.persistentDataPath + "/" + chatFileName;
            Debug.Log("Chat saved: " + path);
            
            File.WriteAllText(path, chatText);
        }

        #endregion
    }
}
