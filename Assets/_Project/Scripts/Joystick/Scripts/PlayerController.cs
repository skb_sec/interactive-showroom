﻿using _Project.Scripts.Conference_Scene;
using UnityEngine;
using UnityEngine.EventSystems;

namespace _Project.Scripts.Joystick.Scripts
{
    public class PlayerController : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler
    {
        public Transform player;
        public CameraController cameraController;
        public RectTransform background;
        public RectTransform pointer;
        public float moveSpeed = 4f;

        public CharacterController m_playerCc;
        private Transform m_cameraTransform;
        private Vector2 m_centerPos;
        private Vector2 m_beginPos;
        [HideInInspector]
        public Vector2 m_dragPos;
        private float m_r;
        public Animator m_anim;
        
        //public AnimationController animationController;
    
        public bool isUserBusy;

        public bool isFirstPersonView;
        
        //Animation States
        // private const string PlayerWalk = "Walk";
        // private const string PlayerIdle = "Idle";

        private void OnEnable()
        {
            player = ReferenceManagerConference.Instance.player.transform;
            
            // m_anim = player.GetComponent<Animator>();
            //
            // animationController = player.GetComponent<AnimationController>();

            // transform.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.height * 0.5f, Screen.height * 0.5f);
            // background.sizeDelta = new Vector2(Screen.height, Screen.height) * 0.25f;
            // pointer.sizeDelta = background.sizeDelta * 0.45f;

            //Charactercontroller parameters
            if (!player.GetComponent<CharacterController>())
            {
                m_playerCc = player.gameObject.AddComponent<CharacterController>();
            }
            else
            {
                m_playerCc = player.GetComponent<CharacterController>();
            }
        
            //m_playerCc.radius = 0.2f;
            //m_playerCc.height = 0.5f;
            //m_playerCc.center = new Vector3(0, 0.15f, 0);
        
            m_cameraTransform = Camera.main.transform;
            m_centerPos = pointer.position;
            m_r = background.sizeDelta.x / 2;
        }

        private Vector2 m_v2;
    
        private void FixedUpdate()
        {
            cameraController.CameraSet();
//             if (!player)
//             {
//                 return;
//             }
//             
//             if (isUserBusy)
//             {
//                 cameraController.CameraSet();
//                 return;
//             }
//             
// #if UNITY_STANDALONE
//             
//             Vector3  moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
//             
//             m_v2 = (m_dragPos - m_beginPos).normalized;
//             float angle = Mathf.Atan2(m_v2.x, m_v2.y) * Mathf.Rad2Deg;
//             angle = angle < 0 ? 360 + angle : angle;
//             m_playerCc.transform.eulerAngles = new Vector3(0, m_cameraTransform.eulerAngles.y + angle, 0);
//             
//             moveDirection = player.TransformDirection(moveDirection);
//             
//             m_playerCc.Move(moveDirection * Time.deltaTime * moveSpeed);
//             
//             if (m_anim)//your ainmation set
//             {
//                 if (moveDirection != Vector3.zero)
//                 {
//                     m_anim.SetBool("walk",true);
//                     //animationController.ChangeAnimationState(PlayerWalk);
//                 }
//                 else
//                 {
//                     m_anim.SetBool("walk",false);
//                     //animationController.ChangeAnimationState(PlayerIdle);
//                 }
//             }
// #else
//             if (!isFirstPersonView)
//             {
//                 if (Vector2.Distance(m_dragPos, m_beginPos) > 0) // joystick move
//                 {
//                     m_v2 = (m_dragPos - m_beginPos).normalized;
//                     float angle = Mathf.Atan2(m_v2.x, m_v2.y) * Mathf.Rad2Deg;
//                     angle = angle < 0 ? 360 + angle : angle;
//                     m_playerCc.transform.eulerAngles = new Vector3(0, m_cameraTransform.eulerAngles.y + angle, 0);
//
//                     m_playerCc.Move(player.forward * Time.deltaTime * moveSpeed);
//         
//                     if (m_anim)//your ainmation set
//                     {
//                         m_anim.SetBool("walk",true);
//                     }
//                 }
//                 else // joystick stop
//                 {
//                     if (m_anim)//your ainmation set
//                     {
//                         m_anim.SetBool("walk",false);
//                     }
//                 }
//             }
//             else
//             {
//                 if (Vector2.Distance(m_dragPos, m_beginPos) > 0) // joystick move
//                 {
//                     m_playerCc.Move(player.forward * Time.deltaTime * moveSpeed);
//         
//                     if (m_anim)//your ainmation set
//                     {
//                         m_anim.SetBool("walk",true);
//                     }
//                 }
//                 else // joystick stop
//                 {
//                     if (m_anim)//your ainmation set
//                     {
//                         m_anim.SetBool("walk",false);
//                     }
//                 }
//                 
//                 m_v2 = (m_dragPos - m_beginPos).normalized;
//                 float angle = Mathf.Atan2(m_v2.x, m_v2.y) * Mathf.Rad2Deg;
//                 angle = angle < 0 ? 360 + angle : angle;
//                 m_playerCc.transform.eulerAngles = new Vector3(0, m_cameraTransform.eulerAngles.y + angle, 0);
//             }
// #endif
//             //Simulated drop
//             if (m_playerCc)
//             {
//                 if (!m_playerCc.isGrounded)
//                 {
//                     m_playerCc.Move(new Vector3(0, -10f * Time.deltaTime, 0));
//                 }
//                 
//                 cameraController.CameraSet();
//             }
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            m_beginPos = eventData.position;
        }

        public void OnDrag(PointerEventData eventData)
        {
            m_dragPos = eventData.position;
            Vector2 dir = m_dragPos - m_beginPos;
            pointer.position = Vector2.Distance(m_dragPos, m_beginPos) > m_r ? (m_centerPos + dir.normalized * m_r) : (m_centerPos + dir);
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            m_dragPos = Vector2.zero;
            m_beginPos = Vector2.zero;
            pointer.position = m_centerPos;
        }
    }
}
