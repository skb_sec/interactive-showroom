﻿using _Project.Scripts.Conference_Scene;
using UnityEngine;
using UnityEngine.EventSystems;

namespace _Project.Scripts.Joystick.Scripts
{
    public class CameraController : MonoBehaviour, IDragHandler,IPointerDownHandler,IPointerUpHandler
    {
        public Transform player;
        //public Texture pointerTex;

        public float piovtY = 0;
        public float distance = 5.0f; // distance from target (used with zoom)
        public float xSpeed = 0.3f;
        public float ySpeed = 0.3f;
        public float yMinLimit = -15f;
        public float yMaxLimit = 90f;

        private Vector3 m_pivotOffset; // offset from target's pivot
        private float m_x = 0;
        private float m_y = 0;
        private float m_targetX = 0;
        private float m_targetY = 0;
        private float m_targetDistance = 0;
        private float m_xVelocity = 1f;
        private float m_yVelocity = 1f;
        private float m_zoomVelocity = 1f;
        private CharacterController m_cameraCc;
        public Transform cameraTransform;

        private int m_firstPointerId = -1;
        private Vector2 m_firstDragPos;
        private float m_firstDragOffset;

        private void Awake()
        {
            RectTransform rt = GetComponent<RectTransform>();
            rt.sizeDelta = new Vector2((int)(Screen.width/2), Screen.height);
        }

        private void OnEnable()
        {
            //Charactercontroller parameters
            //cameraTransform = Camera.main.transform;

            if (!cameraTransform.GetComponent<CharacterController>())
            {
                m_cameraCc = cameraTransform.gameObject.AddComponent<CharacterController>();
            }
            else
            {
                m_cameraCc = cameraTransform.GetComponent<CharacterController>();
            }
        
            m_cameraCc.radius = 0;
            m_cameraCc.height = 0;

            //major parameter
            m_pivotOffset = new Vector3(0, piovtY, 0);
            Vector3 angles = cameraTransform.eulerAngles;
            m_targetX = m_x = angles.x;
            m_targetY = m_y = ClampAngle(angles.y, yMinLimit, yMaxLimit);
            m_targetDistance = distance;
        
            player = ReferenceManagerConference.Instance.player.transform;
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (m_firstPointerId == -1) return;
            m_targetX += eventData.delta.x * xSpeed;
            m_targetY -= eventData.delta.y * ySpeed;
        
            m_targetY = ClampAngle(m_targetY, yMinLimit, yMaxLimit);
        }

        private float ClampAngle(float angle, float min, float max)
        {
            angle= angle < -360 ? angle + 360 : angle;
            angle = angle > 360 ? angle - 360 : angle;
            return Mathf.Clamp(angle, min, max);
        }
        
        public void CameraSet()
        {
            if (!m_cameraCc)
            {
                return;
            }

            if (!player)
            {
                return;
            }
        
            // -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
            m_x = Mathf.SmoothDampAngle(m_x, m_targetX, ref m_xVelocity, 0.3f);
            m_y = Mathf.SmoothDampAngle(m_y, m_targetY, ref m_yVelocity, 0.3f);
            Quaternion rotation = Quaternion.Euler(m_y, m_x, 0);

            distance = Mathf.SmoothDamp(distance, distance, ref m_zoomVelocity, 0.3f);
            Vector3 position = rotation * new Vector3(0.0f, 0.0f, -distance) + player.position + m_pivotOffset;
            cameraTransform.rotation = rotation;

            if (position != cameraTransform.position)
            {
                m_cameraCc.Move(position - cameraTransform.position);
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (m_firstPointerId == -1)
            {
                m_firstPointerId = eventData.pointerId;
            }
        }
    
        public void OnPointerUp(PointerEventData eventData)
        {
            if (eventData.pointerId == m_firstPointerId)
            {
                m_firstPointerId = -1;
            }
        }
    }
}
