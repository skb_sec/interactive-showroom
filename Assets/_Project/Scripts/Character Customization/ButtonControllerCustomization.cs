using _Project.Scripts.Core;
using Photon.Pun;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace _Project.Scripts.Character_Customization
{
    public class ButtonControllerCustomization : MonoBehaviour
    {
        public CharacterCustomization characterCustomization;
        public void LoadConferenceScene()
        {
            characterCustomization.SaveToFile();
            PhotonNetwork.LoadLevel("Conference Scene");
        }

        public void LoadMaleCustomizationScene()
        {
            AppManager.Instance.isMaleAvatar = true;
            SceneManager.LoadScene("Male Customization");
        }
        
        public void LoadFemaleCustomizationScene()
        {
            AppManager.Instance.isMaleAvatar = false;
            SceneManager.LoadScene("Female Customization");
        }
    }
}
