using System.Collections.Generic;
using UnityEngine;

namespace _Project.Scripts.Conference_Scene
{
    public class SpeakerPositionManager : MonoBehaviour
    {
        public Transform speakPos;
        
        public List<AvatarPositions> speakerPos = new List<AvatarPositions>();
        public SpeakerAvatar[] speakerAvatars;

        private Vector3 previousSeatPos;

        public int GetAvailablePos()
        {
            for (int i = 0; i < speakerPos.Count; i++)
            {
                if (!speakerPos[i].unavailabe)
                {
                    return i;
                }
            }

            return -1;
        }
        
        public void RepositionAvatar(Transform parent, int newSeatNo)
        {
            ReferenceManagerConference.Instance.player.transform.localPosition = parent.position;
            ReferenceManagerConference.Instance.player.GetComponent<SpeakerAvatar>().UpdateMySeat(newSeatNo);
        }
        
        public void UpdatePosList()
        {
            Invoke(nameof(UpdateList), 1.0f);
        }
        
        private void UpdateList()
        {
            speakerAvatars = FindObjectsOfType<SpeakerAvatar>();

            ClearAllSeat();
            
            for (int i = 0; i < speakerAvatars.Length; i++)
            {
                int seatIndex = speakerAvatars[i].currentSeatNo;
                
                if (seatIndex >= 0)
                {
                    speakerPos[speakerAvatars[i].currentSeatNo].unavailabe = true;
                }
            }
        }
        
        private void ClearAllSeat()
        {
            for (int i = 0; i < speakerPos.Count; i++)
            {
                speakerPos[i].unavailabe = false;
            }
        }


        public void ToggleSpeakPosAndSeat(bool isOn)
        {
            if (isOn)
            {
                Invoke(nameof(StandUpSpeaker), 1.0f);
                ReferenceManagerConference.Instance.animationController.StandIdle();
            }
            else
            {
                ReferenceManagerConference.Instance.player.transform.position = previousSeatPos;
                ReferenceManagerConference.Instance.animationController.Sit();
            }
        }

        private void StandUpSpeaker()
        {
            previousSeatPos = ReferenceManagerConference.Instance.player.transform.position;
            ReferenceManagerConference.Instance.player.transform.position = speakPos.position;
        }
        
    }
}
