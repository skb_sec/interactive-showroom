using _Project.Scripts.Analytics;
using _Project.Scripts.Core;
using Photon.Pun;
using UnityEngine;

namespace _Project.Scripts.Conference_Scene
{
    public class UiControllerConference : MonoBehaviour
    {
        [SerializeField] private ReferenceManagerConference referenceManagerConference;
        private AnalyticsManager _analyticsManager;

        private string lastPdfLink;


        private void OnEnable()
        {
            referenceManagerConference.playerInstantiate.characterInstantiated += HandleUi;
        }

        private void OnDisable()
        {
            referenceManagerConference.playerInstantiate.characterInstantiated -= HandleUi;
        }

        private void HandleUi()
        {
            referenceManagerConference.controllerCanvas.SetActive(true);
            referenceManagerConference.optionsCanvas.SetActive(true);
        }
        
        private void Start()
        {
            _analyticsManager = FindObjectOfType<AnalyticsManager>();
            switch (AppManager.Instance.currentUserRole)
            {
                case UserRole.Audience:
                    SetupForAudience();
                    break;
                case UserRole.Moderator:
                    SetupForModerator();
                    break;
                case UserRole.Speaker:
                    SetupForSpeaker();
                    break;
                default:
                    SetupForAudience();
                    break;
            }
        }

        private void SetupForAudience()
        {
            DeActiveAll();
            referenceManagerConference.requestToTalkButton.SetActive(true);
            referenceManagerConference.changeSeatButton.gameObject.SetActive(true);
            referenceManagerConference.interactionPanel.SetActive(true);
        }
    
        private void SetupForSpeaker()
        {
            DeActiveAll();
            referenceManagerConference.microphoneButton.SetActive(true);
            referenceManagerConference.conferenceMaterials.SetActive(true);
            referenceManagerConference.speakerStandingPos.gameObject.SetActive(true);
        }
    
        private void SetupForModerator()
        {
            DeActiveAll();
            referenceManagerConference.microphoneButton.SetActive(true);
            referenceManagerConference.allAudienceButton.SetActive(true);
            //referenceManagerConference.interactionPanel.SetActive(true);
            referenceManagerConference.saveChatLogButton.SetActive(true);
        }

        public void StoreChatLog()
        {
            _analyticsManager.SendChatLogAnalytics(ReferenceManagerConference.Instance.chatManager.CurrentChannelText.text);
        }

        public void ShowAudiencePanel()
        {
            referenceManagerConference.allAudiencePanel.SetActive(true);
        }

        public void RequestForMicrophone()
        {
            ReferenceManagerConference.Instance.requestSentMessage.Show();
            ReferenceManagerConference.Instance.player.GetComponent<MicrophonePermissionHandler>().RequestForMicrophone();
        }
        
        public void ShowDisconnectPopup()
        {
            ReferenceManagerConference.Instance.disconnectPopup.Show();
        }

        public void HideDisconnectPopup()
        {
            ReferenceManagerConference.Instance.disconnectPopup.Hide();
            Invoke(nameof(QuitApp), 1.0f);
        }
        
        public void ShowSameUserExistPopup()
        {
            ReferenceManagerConference.Instance.sameUserExistPopup.Show();
        }
        
        public void HideSameUserExistPopup()
        {
            ReferenceManagerConference.Instance.sameUserExistPopup.Hide();
            Invoke(nameof(QuitApp), 1.0f);
        }
        
        
        public void SetMicUserName(string micUserName)
        {
            ReferenceManagerConference.Instance.micUsernameText.text = micUserName;
        }


        #region PdfPresentation
        public void LoadPdf(string link)
        {
            ReferenceManagerConference.Instance.presentationCanvasScreen.SetActive(true);
            ReferenceManagerConference.Instance.pdfController.LoadDocumentFromLink(link);
        }
        
        public void LoadWorldPdf(string link)
        {
            lastPdfLink = link;
            ReferenceManagerConference.Instance.presentationCanvasWorld.SetActive(true);
            ReferenceManagerConference.Instance.pdfController.LoadDocumentInWorldFromLink(link);
        }

        public void ExpandWorldPdfToScreen()
        {
            LoadPdf(lastPdfLink);
        }

        public void LoadRemotePdf(string link)
        {
           // ReferenceManagerConference.Instance.player.GetComponent<MultiplayerPresentationManager>().LoadPdfPresentation(link);
        }

        public void CloseWorldPdf()
        {
            ReferenceManagerConference.Instance.presentationCanvasWorld.SetActive(false);
        }
        
        public void CloseScreenPdf()
        {
            ReferenceManagerConference.Instance.presentationCanvasScreen.SetActive(false);
        }

        public void CloseRemotePdf()
        {
            //ReferenceManagerConference.Instance.player.GetComponent<MultiplayerPresentationManager>().ClosePdfPresentation();
        }
        #endregion

        
        
        #region Presentation

        public void ShowSlide(string url)
        {
            ReferenceManagerConference.Instance.presentation.SetActive(true);
            ReferenceManagerConference.Instance.slideObject.SetActive(true);
            ReferenceManagerConference.Instance.videoObject.gameObject.SetActive(false);
            ReferenceManagerConference.Instance.imageLoader.LoadImageInUi(ReferenceManagerConference.Instance.worldSlideImage, url);
        }
        
        public void ShowVideo(string url)
        {
            ReferenceManagerConference.Instance.presentation.SetActive(true);
            ReferenceManagerConference.Instance.slideObject.SetActive(false);
            ReferenceManagerConference.Instance.videoObject.gameObject.SetActive(true);
            ReferenceManagerConference.Instance.videoObject.url = url;
            ReferenceManagerConference.Instance.videoObject.Play();
        }

        public void HideRemotePresentation()
        {
            ReferenceManagerConference.Instance.player.GetComponent<MultiplayerPresentationManager>().ClosePresentation();
        }
        
        public void HidePresentation()
        {
            ReferenceManagerConference.Instance.presentation.SetActive(false);
            ReferenceManagerConference.Instance.slideObject.SetActive(false);
            ReferenceManagerConference.Instance.videoObject.gameObject.SetActive(false);
            ReferenceManagerConference.Instance.presentationCamera.SetActive(false);
            ReferenceManagerConference.Instance.generalCanvas.SetActive(true);
        }
        
        public void ShowPresentationCloseView()
        {
            ReferenceManagerConference.Instance.presentationCamera.SetActive(!ReferenceManagerConference.Instance.presentationCamera.activeSelf);
            ReferenceManagerConference.Instance.generalCanvas.SetActive(!ReferenceManagerConference.Instance.presentationCamera.activeSelf);
        }

        #endregion
        

        private void QuitApp()
        {
            Application.Quit();
        }

        private void DeActiveAll()
        {
            referenceManagerConference.allAudiencePanel.SetActive(false);
            referenceManagerConference.allAudienceButton.SetActive(false);
            referenceManagerConference.microphoneButton.SetActive(false);
            referenceManagerConference.conferenceMaterials.SetActive(false);
            referenceManagerConference.speakerStandingPos.gameObject.SetActive(false);
            referenceManagerConference.requestToTalkButton.SetActive(false);
            referenceManagerConference.changeSeatButton.gameObject.SetActive(false);
            referenceManagerConference.interactionPanel.SetActive(false);
            referenceManagerConference.saveChatLogButton.SetActive(false);
        }

        public void OnAllAudienceToggleClick(bool isOn)
        {
            ReferenceManagerConference.Instance.allAudienceListPanel.SetActive(isOn);
        }

        public void OnRequestingMicToggleClick(bool isOn)
        {
            ReferenceManagerConference.Instance.requestingMicAudienceListPanel.SetActive(isOn);
        }

        public void ShowPresentation()
        {
            PhotonNetwork.Instantiate(ReferenceManagerConference.Instance.presentationName, Vector3.zero, Quaternion.identity);
        }
    }
}
