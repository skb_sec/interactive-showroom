using _Project.Scripts.Core;
using JetBrains.Annotations;
using Photon.Pun;
using UnityEngine;

namespace _Project.Scripts.Conference_Scene
{
    public class AudienceAvatar : MonoBehaviour
    {
        public GameObject avatar;
        public int currentSeatNo;
        private PhotonView m_pv;
        
        private void Start()
        {
            m_pv = GetComponent<PhotonView>();
            
            if (m_pv.IsMine)
            {
                if (AppManager.Instance.currentUserRole == UserRole.Moderator)
                {
                    m_pv.RPC(nameof(RPC_HideAvatar), RpcTarget.AllBuffered);
                }
                else
                {
                    Invoke(nameof(DecideInitialSeatAndUpdatePos), 2.0f);   
                }
            }
        }

        [PunRPC]
        private void RPC_HideAvatar()
        {
            avatar.SetActive(false);
        }
        
        
        private void DecideInitialSeatAndUpdatePos()
        {
            int availablePos = ReferenceManagerConference.Instance.avatarPositioner.GetAvailablePos();
            
            if (availablePos == -1)
            {
                Debug.Log("No Available Pos");
            }

            else
            {
                ReferenceManagerConference.Instance.avatarPositioner.
                    RepositionAvatar(ReferenceManagerConference.Instance.avatarPositioner.audiencePos[availablePos].posTransform, availablePos);
            }
        }

        public void UpdateMySeat(int newSeatNumber)
        {
            m_pv.RPC(nameof(RPC_SetSeatNumber), RpcTarget.AllBuffered, newSeatNumber);
        }
        
        [PunRPC]
        private void RPC_SetSeatNumber(int _currentSeatNo)
        {
            currentSeatNo = _currentSeatNo;

            ReferenceManagerConference.Instance.avatarPositioner.UpdatePosList();
            ReferenceManagerConference.Instance.speakerPositioner.UpdatePosList();
        }
    }
}
