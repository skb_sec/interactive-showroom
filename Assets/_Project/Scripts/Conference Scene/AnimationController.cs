using System.Collections;
using System.Collections.Generic;
using _Project.Scripts.Character_Customization;
using _Project.Scripts.Core;
using UnityEngine;

namespace _Project.Scripts.Conference_Scene
{
    public class AnimationController : MonoBehaviour
    {
        private CharacterCustomization m_characterCustomization;
        
        private readonly Dictionary<string, string> animatorTriggers = new Dictionary<string, string>();

        private void Awake()
        {
            animatorTriggers.Add("clap", "clap");
            animatorTriggers.Add("question", "question");
            animatorTriggers.Add("thumbsup", "thumbsup");
            animatorTriggers.Add("talking", "talking");
            animatorTriggers.Add("disbelief", "disbelief");
            animatorTriggers.Add("sitting", "sitting");
            animatorTriggers.Add("standing idle", "standing idle");
        }
        
        public void InitializeAnimation(CharacterCustomization characterCustomization)
        {
            m_characterCustomization = characterCustomization;
            
            Sit();
        }

        public void Walk()
        {
            foreach (Animator a in m_characterCustomization.GetAnimators())
                a.SetBool("walk", true);
        }
        
        public void StandIdle()
        {
            StartCoroutine(EnableAnimation("standing idle"));
            
            CancelInvoke(nameof(Sit));
        }


        public void Clap()
        {
            StartCoroutine(EnableAnimation("clap"));
            
            CancelInvoke(nameof(Sit));
            Invoke(nameof(Sit), 5);
        }

        public void Question()
        {
            StartCoroutine(EnableAnimation("question"));
            
            CancelInvoke(nameof(Sit));
            Invoke(nameof(Sit), 5);
        }

        public void ThumbsUp()
        {
            StartCoroutine(EnableAnimation("thumbsup"));
            
            CancelInvoke(nameof(Sit));
            Invoke(nameof(Sit), 5);
        }

        public void StartTalkingAnim()
        {
            StartCoroutine(EnableAnimation("talking"));
        }

        public void StopTalkingAnim()
        {
            CancelInvoke(nameof(Sit));
            Sit();
        }

        public void Disbelief()
        {
            StartCoroutine(EnableAnimation("disbelief"));
            CancelInvoke(nameof(Sit));
            Invoke(nameof(Sit), 5);
        }

        public void Sit()
        {
            Debug.Log("In Seat");
            StartCoroutine(EnableAnimation("sitting"));
        }

        private IEnumerator EnableAnimation(string triggerName)
        {
            ResetAll();
            yield return new WaitForSeconds(0.1f);

            foreach (Animator a in m_characterCustomization.GetAnimators())
                a.SetBool(triggerName, true);
        }

        private void ResetAll()
        {
            foreach (var animatorTrigger in animatorTriggers)
            {
                foreach (Animator a in m_characterCustomization.GetAnimators())
                    a.SetBool(animatorTrigger.Value, false);
            }
        }
    }
}