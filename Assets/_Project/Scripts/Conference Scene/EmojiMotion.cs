using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Conference_Scene
{
    public class EmojiMotion : MonoBehaviour
    {
        public Image emoji;
        public float speed;

        void Start()
        {
            Invoke(nameof(Animate), 0.5f);
        }
        
        private void Update()
        {
            Vector3 pos = transform.position;
            pos.y = pos.y + (speed * Time.deltaTime);
        
            transform.position = pos;
        }

        private void Animate()
        {
            transform.DOScale(transform.localScale/10, 0.2f);
            Invoke(nameof(DestroyItself), 0.25f);
        }
        
        private void DestroyItself()
        {
            Destroy(gameObject);
        }
    }
}
