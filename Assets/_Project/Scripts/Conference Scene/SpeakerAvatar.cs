using Photon.Pun;
using UnityEngine;

namespace _Project.Scripts.Conference_Scene
{
    public class SpeakerAvatar : MonoBehaviour
    {
        public int currentSeatNo;
        private PhotonView m_pv;

        private void Start()
        {
            m_pv = GetComponent<PhotonView>();
            
            if (m_pv.IsMine)
            {
                Invoke(nameof(DecideInitialSeatAndUpdatePos), 2.0f);
            }
        }

        private void DecideInitialSeatAndUpdatePos()
        {
            int availablePos = ReferenceManagerConference.Instance.speakerPositioner.GetAvailablePos();
            
            if (availablePos == -1)
            {
                Debug.Log("No Available Pos");
            }

            else
            {
                ReferenceManagerConference.Instance.speakerPositioner.
                    RepositionAvatar(ReferenceManagerConference.Instance.speakerPositioner.speakerPos[availablePos].posTransform, availablePos);
            }
        }
        
        public void UpdateMySeat(int newSeatNumber)
        {
            m_pv.RPC("RPC_SetSeatNumber", RpcTarget.AllBuffered, newSeatNumber);
        }
        
        [PunRPC]
        private void RPC_SetSeatNumber(int _currentSeatNo)
        {
            currentSeatNo = _currentSeatNo;

            ReferenceManagerConference.Instance.speakerPositioner.UpdatePosList();
        }
    }
}
