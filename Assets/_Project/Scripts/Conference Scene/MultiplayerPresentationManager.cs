using Photon.Chat;
using Photon.Pun;
using UnityEngine;

namespace _Project.Scripts.Conference_Scene
{
    public class MultiplayerPresentationManager : MonoBehaviour
    {
        private PhotonView PV;
        private void Start()
        {
            PV = GetComponent<PhotonView>();
        }
        
        #region Presentation

        public void LoadSlidePresentation(string link)
        {
            PV.RPC("RPC_LoadSlidePresentation", RpcTarget.OthersBuffered, link);
        }

        [PunRPC]
        private void RPC_LoadSlidePresentation(string link)
        {
            ReferenceManagerConference.Instance.uiController.ShowSlide(link);
        }
        
        
        public void LoadVideoPresentation(string link)
        {
            PV.RPC("RPC_LoadVideoPresentation", RpcTarget.OthersBuffered, link);
        }

        [PunRPC]
        private void RPC_LoadVideoPresentation(string link)
        {
            ReferenceManagerConference.Instance.uiController.ShowVideo(link);
        }
        
        
        public void ClosePresentation()
        {
            PV.RPC("RPC_ClosePresentation", RpcTarget.OthersBuffered);
        }
        
        [PunRPC]
        private void RPC_ClosePresentation()
        {
            ReferenceManagerConference.Instance.uiController.HidePresentation();
        }

        #endregion

    }
}