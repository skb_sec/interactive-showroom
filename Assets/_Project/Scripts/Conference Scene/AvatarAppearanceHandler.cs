﻿using System.IO;
using _Project.Scripts.Character_Customization;
using _Project.Scripts.Core;
using AdvancedCustomizableSystem;
using Photon.Pun;
using TMPro;
using UnityEngine;

namespace _Project.Scripts.Conference_Scene
{
    public class AvatarAppearanceHandler : MonoBehaviour
    {
        [Header("Avatar Appearance Variables")]
        public CharacterCustomizationSetup avatarAppearanceValue;

        public GameObject infoPanel;
        public int id;
        public TMP_Text userName;
        public TMP_Text company;
        public TMP_Text position;

        private PhotonView PV;
        private GameObject m_player;

        private void Start()
        {
            PV = GetComponent<PhotonView>();
           Appear();
           UserInfo();
        }
        
        void Appear()
        {
            if (PV.IsMine)
            {
                string name = PhotonNetwork.NickName;
                
                CharacterCustomizationSetup aValue = new CharacterCustomizationSetup();
                string fileName = AppManager.Instance.isMaleAvatar ? "Male Avatar" : "Female Avatar";
                aValue = LoadFromFile(fileName);

                PV.RPC(nameof(RPC_AvatarAppearance), RpcTarget.AllBuffered, aValue.Hair, aValue.Hat, 
                    aValue.TShirt, aValue.Pants, aValue.Shoes, aValue.Beard, aValue.Accessory, aValue.Fat, aValue.Muscles, aValue.Slimness, aValue.Thin, aValue.BreastSize, 
                    aValue.Neck_Width, aValue.Ear_Size, aValue.Ear_Angle, aValue.Jaw_Width, aValue.Jaw_Shift, aValue.Jaw_Offset, aValue.Cheek_Size, aValue.Chin_Offset, 
                    aValue.Eye_Width, aValue.Eye_Form, aValue.Eye_InnerCorner, aValue.Eye_Corner, aValue.Eye_Rotation, aValue.Eye_Offset, aValue.Eye_ScaleX, aValue.Eye_ScaleY, 
                    aValue.Eye_Size, aValue.Eye_Close, aValue.Eye_Height, aValue.Brow_Height, aValue.Brow_Shape, aValue.Brow_Thickness, aValue.Brow_Length, aValue.Nose_Length, 
                    aValue.Nose_Size, aValue.Nose_Angle, aValue.Nose_Offset, aValue.Nose_Bridge, aValue.Nose_Hump, aValue.Mouth_Offset, aValue.Mouth_Width, aValue.Mouth_Size, 
                    aValue.Mouth_Open, aValue.Mouth_Bulging, aValue.LipsCorners_Offset, aValue.Face_Form, aValue.Chin_Width, aValue.Chin_Form, aValue.Head_Offset, aValue.Height, 
                    aValue.Smile, aValue.Sadness, aValue.Surprise, aValue.Thoughtful, aValue.Angry, aValue.HeadSize, aValue.SkinColor.r, aValue.SkinColor.g, aValue.SkinColor.b);
            }
        }
        

        [PunRPC]
        private void RPC_AvatarAppearance(int Hair, int Hat, int TShirt, int Pants, int Shoes, int Beard, int Accessory, float Fat, float Muscles, float Slimness, float Thin, 
            float BreastSize, float Neck_Width, float Ear_Size, float Ear_Angle, float Jaw_Width, float Jaw_Shift, float Jaw_Offset, float Cheek_Size, float Chin_Offset, 
            float Eye_Width, float Eye_Form, float Eye_InnerCorner, float Eye_Corner, float Eye_Rotation, float Eye_Offset, float Eye_ScaleX, float Eye_ScaleY, float Eye_Size, 
            float Eye_Close, float Eye_Height, float Brow_Height, float Brow_Shape, float Brow_Thickness, float Brow_Length, float Nose_Length, float Nose_Size, 
            float Nose_Angle, float Nose_Offset, float Nose_Bridge, float Nose_Hump, float Mouth_Offset, float Mouth_Width, float Mouth_Size, float Mouth_Open, float Mouth_Bulging, 
            float LipsCorners_Offset, float Face_Form, float Chin_Width, float Chin_Form, float Head_Offset, float Height, float Smile, float Sadness, float Surprise, 
            float Thoughtful, float Angry, float HeadSize, float SkinColorR,  float SkinColorG,  float SkinColorB)
        {
            avatarAppearanceValue.Hair = Hair;
            avatarAppearanceValue.Hat = Hat;
            avatarAppearanceValue.TShirt = TShirt;
            avatarAppearanceValue.Pants = Pants;
            avatarAppearanceValue.Shoes = Shoes;
            avatarAppearanceValue.Beard = Beard;
            avatarAppearanceValue.Accessory = Accessory;

            avatarAppearanceValue.Fat = Fat;
            avatarAppearanceValue.Muscles = Muscles;
            avatarAppearanceValue.Slimness = Slimness;
            avatarAppearanceValue.Thin = Thin;
            avatarAppearanceValue.BreastSize = BreastSize;
            avatarAppearanceValue.Neck_Width = Neck_Width;
            avatarAppearanceValue.Ear_Size = Ear_Size;
            avatarAppearanceValue.Ear_Angle = Ear_Angle;
            avatarAppearanceValue.Jaw_Width = Jaw_Width;
            avatarAppearanceValue.Jaw_Shift = Jaw_Shift;
            avatarAppearanceValue.Jaw_Offset = Jaw_Offset;
            avatarAppearanceValue.Cheek_Size = Cheek_Size;
            avatarAppearanceValue.Chin_Offset = Chin_Offset;
            avatarAppearanceValue.Eye_Width = Eye_Width;
            avatarAppearanceValue.Eye_Form = Eye_Form;
            avatarAppearanceValue.Eye_InnerCorner = Eye_InnerCorner;
            avatarAppearanceValue.Eye_Corner = Eye_Corner;
            avatarAppearanceValue.Eye_Rotation = Eye_Rotation;
            avatarAppearanceValue.Eye_Offset = Eye_Offset;
            avatarAppearanceValue.Eye_ScaleX = Eye_ScaleX;
            avatarAppearanceValue.Eye_ScaleY = Eye_ScaleY;
            avatarAppearanceValue.Eye_Size = Eye_Size;
            avatarAppearanceValue.Eye_Close = Eye_Close;
            avatarAppearanceValue.Eye_Height = Eye_Height;
            avatarAppearanceValue.Brow_Height = Brow_Height;
            avatarAppearanceValue.Brow_Shape = Brow_Shape;
            avatarAppearanceValue.Brow_Thickness = Brow_Thickness;
            avatarAppearanceValue.Brow_Length = Brow_Length;
            avatarAppearanceValue.Nose_Length = Nose_Length;
            avatarAppearanceValue.Nose_Size = Nose_Size;
            avatarAppearanceValue.Nose_Angle = Nose_Angle;
            avatarAppearanceValue.Nose_Offset = Nose_Offset;
            avatarAppearanceValue.Nose_Bridge = Nose_Bridge;
            avatarAppearanceValue.Nose_Hump = Nose_Hump;
            avatarAppearanceValue.Mouth_Offset = Mouth_Offset;
            avatarAppearanceValue.Mouth_Width = Mouth_Width;
            avatarAppearanceValue.Mouth_Size = Mouth_Size;
            avatarAppearanceValue.Mouth_Open = Mouth_Open;
            avatarAppearanceValue.Mouth_Bulging = Mouth_Bulging;
            avatarAppearanceValue.LipsCorners_Offset = LipsCorners_Offset;
            avatarAppearanceValue.Face_Form = Face_Form;
            avatarAppearanceValue.Chin_Width = Chin_Width;
            avatarAppearanceValue.Chin_Form = Chin_Form;
            avatarAppearanceValue.Head_Offset = Head_Offset;
            avatarAppearanceValue.Height = Height;
            avatarAppearanceValue.Smile = Smile;
            avatarAppearanceValue.Sadness = Sadness;
            avatarAppearanceValue.Surprise = Surprise;
            avatarAppearanceValue.Thoughtful = Thoughtful;
            avatarAppearanceValue.Angry = Angry;
            avatarAppearanceValue.HeadSize = HeadSize;
            avatarAppearanceValue.SkinColor = new Color(SkinColorR, SkinColorG, SkinColorB, 1.0f);


            LoadAvatarFromInfo(GetComponent<CharacterCustomization>());
        }


        private void LoadAvatarFromInfo(CharacterCustomization characterCustomizer)
        {
            Debug.Log(characterCustomizer.name);
            characterCustomizer.SetCharacterSetup(avatarAppearanceValue);
        }
        
        private CharacterCustomizationSetup LoadFromFile(string fileName)
        {
            CharacterCustomizationSetup characterCustomizationSetup = new CharacterCustomizationSetup();
            
            var dataPath = Application.persistentDataPath;
            var folderPath = string.Format("{0}/{1}", dataPath, "charactersData");
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            else
            {

                var filePath = $"{folderPath}/characterData_{fileName}.json";
                
                if (File.Exists(filePath))
                {
                    string jsonObject = File.ReadAllText(filePath);
                    if (jsonObject != string.Empty)
                    {
                        characterCustomizationSetup = CharacterCustomizationSetup.DeserializeFromJson(jsonObject);
                        //SetCharacterSetup(characterCustomizationSetup);
                    }
                }
            }
            return characterCustomizationSetup;
        }


        private void UserInfo()
        {
            if (PV.IsMine)
            {
                infoPanel.SetActive(false);
                
                bool isModerator = AppManager.Instance.currentUserRole == UserRole.Moderator;
                
                PV.RPC(nameof(RPC_UserInfo), RpcTarget.AllBuffered,
                    AppManager.Instance.userInfo.UserData[0].name, AppManager.Instance.userInfo.UserData[0].company, 
                    AppManager.Instance.userInfo.UserData[0].position, AppManager.Instance.userInfo.UserData[0].id, isModerator);
            }
        }

        [PunRPC]
        private void RPC_UserInfo(string userName, string company, string position, int id, bool isModerator)
        {
            this.userName.text = "Name: " + userName;
            this.company.text = "Company: " + company;
            this.position.text = "Designation: " + position;
            this.id = id;

            Debug.Log("isModerator: "+isModerator);
            if (isModerator)
            {
                infoPanel.SetActive(false);
            }
            CheckExistingUser();
        }
        
        private void CheckExistingUser()
        {
            int count = 0;
            
            ReferenceManagerConference.Instance.allAvatar = FindObjectsOfType<AvatarAppearanceHandler>();
            for (int i = 0; i < ReferenceManagerConference.Instance.allAvatar.Length; i++)
            {
                Debug.Log(ReferenceManagerConference.Instance.allAvatar[i].id);
                
                if (id == ReferenceManagerConference.Instance.allAvatar[i].id)
                {
                    count++;
                }

                if (count > 1)
                {
                    ReferenceManagerConference.Instance.uiController.ShowSameUserExistPopup();
                }
            }
        }
    }
}