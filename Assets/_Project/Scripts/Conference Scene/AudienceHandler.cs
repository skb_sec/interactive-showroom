using System;
using System.Collections.Generic;
using UnityEngine;

namespace _Project.Scripts.Conference_Scene
{
    public class AudienceHandler : MonoBehaviour
    {
        public List<AudienceProperty> allAudience;
        public List<Audience> currentAudiences;
        public List<Audience> requestingMicAudience;

        private void Awake()
        {
            ReferenceManagerConference.Instance.requestingAudienceCount.text = "Mic Request(" + ReferenceManagerConference.Instance.requestingAudienceParent.childCount + ")";
            ReferenceManagerConference.Instance.allAudienceCount.text = "All Audience(" + allAudience.Count + ")";
        }

        public void UpdateAudienceList()
        {
            LoadAudience();
        }

        private void LoadAudience()
        {
            ClearAudience();
            
            if (allAudience != null)
            {
                ReferenceManagerConference.Instance.allAudienceCount.text = "All Audience(" + allAudience.Count + ")";
                
                for (int i = 0; i < allAudience.Count; i++)
                {
                    Audience audience = Instantiate(ReferenceManagerConference.Instance.audiencePrefab, 
                        ReferenceManagerConference.Instance.allAudienceParent).GetComponent<Audience>();
                    
                    audience.audienceName.text = allAudience[i].audienceName;
                    audience.userId = allAudience[i].userId;
                    audience.microphoneToggle.gameObject.SetActive(false);
                    audience.removeButton.gameObject.SetActive(false);
                    
                    currentAudiences.Add(audience);
                }
            }
        }
        
        public void AddAudienceInRequesting(int userId, string audienceName)
        {
            if (ExistingRequest(userId))
            {
                Debug.Log("Existing request. Return.");
                return;
            }
            
            Audience audience = Instantiate(ReferenceManagerConference.Instance.audiencePrefab, 
                ReferenceManagerConference.Instance.requestingAudienceParent).GetComponent<Audience>();
                    
            audience.audienceName.text = audienceName;
            audience.userId = userId;
            audience.requesting.SetActive(true);
            
            requestingMicAudience.Add(audience);
            
            ReferenceManagerConference.Instance.requestingAudienceCount.text = "Mic Request(" + ReferenceManagerConference.Instance.requestingAudienceParent.childCount + ")";
        }

        private bool ExistingRequest(int userId)
        {
            for (int i = 0; i < requestingMicAudience.Count; i++)
            {
                if (requestingMicAudience[i].userId == userId)
                {
                   return true;
                }
            }

            return false;
        }

        public void RemoveAudienceFromRequesting(Audience audience)
        {
            if (requestingMicAudience.Contains(audience))
            {
                requestingMicAudience.Remove(audience);
                Debug.Log("Removed From Requesting List");
            }
        }

        private void ClearAudience()
        {
            currentAudiences.Clear();
            
            foreach (Transform child in ReferenceManagerConference.Instance.allAudienceParent)
            {
                Destroy(child.gameObject);
            }
        }
    }
}
