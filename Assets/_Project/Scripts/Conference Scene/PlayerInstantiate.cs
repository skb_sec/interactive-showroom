using System;
using _Project.Scripts.Character_Customization;
using _Project.Scripts.Core;
using Photon.Pun;
using UnityEngine;

namespace _Project.Scripts.Conference_Scene
{
    public class PlayerInstantiate : MonoBehaviour
    {
        public MultiplayerController multiplayerController;
        public GameObject maleAudiencePrefab;
        public GameObject femaleAudiencePrefab;
        public GameObject maleSpeakerPrefab;
        public GameObject femaleSpeakerPrefab;
        
        public Action characterInstantiated;
        private GameObject player;

        private void OnEnable()
        {
            multiplayerController.ONJoinedRoomCallback += InstantiatePlayer;
        }

        private void OnDisable()
        {
            multiplayerController.ONJoinedRoomCallback -= InstantiatePlayer;
        }

        private void InstantiatePlayer()
        {
            string avatar;
            
            if (AppManager.Instance.currentUserRole == UserRole.Speaker)
            {
                avatar = AppManager.Instance.isMaleAvatar ? maleSpeakerPrefab.name : femaleSpeakerPrefab.name;
            }
            else
            {
                avatar = AppManager.Instance.isMaleAvatar ? maleAudiencePrefab.name : femaleAudiencePrefab.name;
            }

            PositionRotation positionRotation = ReferenceManagerConference.Instance.avatarPositioner.PlayerInstantiatePositionRotation();
            
            player = PhotonNetwork.Instantiate(avatar, positionRotation.positionVector, positionRotation.rotationVector);

            ReferenceManagerConference.Instance.player = player;
            ReferenceManagerConference.Instance.emojiManager.PositionEmojiCanvas();
            ReferenceManagerConference.Instance.animationController.InitializeAnimation(player.GetComponent<CharacterCustomization>());
            characterInstantiated?.Invoke();
        }
    }
}
