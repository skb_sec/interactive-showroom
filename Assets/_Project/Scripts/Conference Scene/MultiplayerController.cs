using System;
using System.Collections.Generic;
using _Project.Scripts.Core;
using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using Random = UnityEngine.Random;

namespace _Project.Scripts.Conference_Scene
{
    public class MultiplayerController : MonoBehaviourPunCallbacks
    {
        private RoomOptions _roomOptions;
        private int _counter;

        public Action ONJoinedRoomCallback;

        private Hashtable _customProperties;
        
        private string _sceneName;

        private void Awake()
        {
            PhotonNetwork.AutomaticallySyncScene = true;
        }

        private void Start()
        {
            StartConnectingToPhoton("Virtual Conference ");
        }

        private void StartConnectingToPhoton(string filterParam)
        {
            //sceneLoadManager.uiManagerSceneLoad.loadCircle.Show();
            SetCustomProperty(filterParam);
            PreparePhotonAndJoinRoom();
        }

        private void SetCustomProperty(string _sceneName)
        {
            this._sceneName = _sceneName;
            
            string customEvent = AppManager.Instance.userInfo.UserData[0].event_name +
                                 AppManager.Instance.userInfo.UserData[0].event_id;
            
            //Hashtable customProperties = new Hashtable {["Scene"] = customEvent + sceneName};
            _customProperties = new Hashtable {["Scene"] = customEvent + this._sceneName};

            _roomOptions = new RoomOptions
            {
                CustomRoomProperties = _customProperties,
                IsVisible = true,
                IsOpen = true,
                MaxPlayers = AppManager.MAX_PLAYER_IN_PHOTON_ROOM,
                CleanupCacheOnLeave = true,
                CustomRoomPropertiesForLobby = new string[] {"Scene",}
            };

            PhotonNetwork.AutomaticallySyncScene = false;
        }

        private void PreparePhotonAndJoinRoom()
        {
            PhotonNetwork.NickName = AppManager.Instance.userInfo.UserData[0].name;
            if (!PhotonNetwork.IsConnected)
            {
                PhotonNetwork.GameVersion = AppManager.Instance.gameVersion;
                PhotonNetwork.ConnectUsingSettings();
            }
            else
            {
                if (PhotonNetwork.InRoom)
                {
                    PhotonNetwork.LeaveRoom();
                }
                else if (PhotonNetwork.IsConnectedAndReady)
                {
                    PhotonNetwork.JoinLobby(TypedLobby.Default);
                }
                else
                {
                    PhotonNetwork.GameVersion = AppManager.Instance.gameVersion;
                    PhotonNetwork.ConnectUsingSettings();
                }
            }
        }

        public override void OnJoinedLobby()
        {
            Debug.Log("Joined to the lobby");
            if (!PhotonNetwork.InRoom)
            {
                Debug.Log("Joining Room...");

                PhotonNetwork.JoinRandomRoom(_customProperties, AppManager.MAX_PLAYER_IN_PHOTON_ROOM);
            }
        }

        public override void OnConnectedToMaster()
        {
            Debug.Log("Connected to Master");
            PhotonNetwork.JoinLobby(TypedLobby.Default);
        }

        public override void OnJoinRandomFailed(short returnCode, string message)
        {
            if (_counter <= 1)
            {
                Debug.Log("Try creating room");
                CreateRoom();
                _counter++;
            }
            else
            {
                _counter = 0;
                message = "Server not responding. Please try again later.";
                ReferenceManagerConference.Instance.uiController.ShowDisconnectPopup();
            }
        }

        private void CreateRoom()
        {
            var randomRoomNumber = Random.Range(0, 10000);
            PhotonNetwork.CreateRoom(_sceneName + randomRoomNumber, _roomOptions);
        }

        public override void OnCreateRoomFailed(short returnCode, string message)
        {
            Debug.Log("Room creation failed. There may already exist room with same name.");
            if (_counter <= 1)
            {
                Debug.Log("Try room creation again");
                CreateRoom();
                _counter++;
            }
            else
            {
                _counter = 0;
                message = "Server is not responding. Please try again later.";
                ReferenceManagerConference.Instance.uiController.ShowDisconnectPopup();
            }
        }

        public override void OnDisconnected(DisconnectCause cause)
        {
            ReferenceManagerConference.Instance.uiController.ShowDisconnectPopup();
        }

        public override void OnPlayerEnteredRoom(Player newPlayer)
        {
            UpdateFriendList();
        }

        public override void OnPlayerLeftRoom(Player otherPlayer)
        {
            UpdateFriendList();
        }

        public override void OnJoinedRoom()
        {
            Debug.Log("Joined in room: " + PhotonNetwork.CurrentRoom.Name);
            if (AppManager.Instance.currentUserRole == UserRole.Moderator)
            {
                UpdateFriendList();
            }
            ONJoinedRoomCallback?.Invoke();
        }

        private void UpdateFriendList()
        {
            List<AudienceProperty> audienceList = new List<AudienceProperty>();

            for (int i = 0; i < PhotonNetwork.PlayerListOthers.Length; i++)
            {
                if (!PhotonNetwork.PlayerListOthers[i].IsInactive)
                {
                    Debug.Log(PhotonNetwork.PlayerListOthers[i].NickName);
                    AudienceProperty audience = new AudienceProperty();
                    audience.audienceName = PhotonNetwork.PlayerListOthers[i].NickName;
                    audience.userId = PhotonNetwork.PlayerListOthers[i].ActorNumber;

                    Debug.Log(audience.userId);

                    audienceList.Add(audience);
                }
            }

            ReferenceManagerConference.Instance.audienceHandler.allAudience = audienceList;

            ReferenceManagerConference.Instance.audienceHandler.UpdateAudienceList();

            ReferenceManagerConference.Instance.avatarPositioner.UpdatePosList();
            ReferenceManagerConference.Instance.speakerPositioner.UpdatePosList();
        }
    }

    [Serializable]
    public class AudienceProperty
    {
        public string audienceName;
        public int userId;
    }
}