using UnityEngine;

namespace _Project.Scripts.Conference_Scene
{
    public class AppSleepTimer : MonoBehaviour
    {
        void Start()
        {
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
        }
    }
}
