﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Conference_Scene
{
    public class Audience : MonoBehaviour
    {
        public TMP_Text audienceName;
        public GameObject requesting;
        public Toggle microphoneToggle;
        public Button removeButton;

        public int userId;

        private void Start()
        {
            microphoneToggle.onValueChanged.AddListener(MicrophonePermission);
        }

        private void OnDisable()
        {
            microphoneToggle.onValueChanged.RemoveListener(MicrophonePermission);
        }
        

        public void HandleIfRequestedUi(int userId)
        {
            if (this.userId == userId)
            {
                //requesting.SetActive(true);
                Debug.Log("HandleIfRequestedUi: " + userId);
                ReferenceManagerConference.Instance.audienceHandler.AddAudienceInRequesting(userId, audienceName.text);
            }
        }

        public void Remove()
        {
            if (microphoneToggle.isOn)
            {
                ReferenceManagerConference.Instance.player.GetComponent<MicrophonePermissionHandler>().CloseTargetMicrophone(userId);
            }
            ReferenceManagerConference.Instance.audienceHandler.RemoveAudienceFromRequesting(this);
            Invoke(nameof(SelfDestroy), 0.5f);
        }

        private void SelfDestroy()
        {
            Destroy(gameObject);
        }

        private void MicrophonePermission(bool isOn)
        {
            requesting.SetActive(false);
            
            if (isOn)
            {
                ReferenceManagerConference.Instance.player.GetComponent<MicrophonePermissionHandler>().AllowTargetMicrophone(userId);
            }
            else
            {
                ReferenceManagerConference.Instance.player.GetComponent<MicrophonePermissionHandler>().CloseTargetMicrophone(userId);
            }
        }
        
    }
}
