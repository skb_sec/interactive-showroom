﻿using _Project.Scripts.Analytics;
using _Project.Scripts.Core;
using Photon.Pun;
using UnityEngine;

namespace _Project.Scripts.Conference_Scene
{
    public class MicrophonePermissionHandler: MonoBehaviour
    {
        private PhotonView _pv;
        //private ReferenceManagerConference referenceManagerConference;
        private AnalyticsManager _analyticsManager;

        // public MicrophonePermissionHandler(ReferenceManagerConference referenceManagerConference)
        // {
        //     this.referenceManagerConference = referenceManagerConference;
        // }

        private void Start()
        {
            _analyticsManager = FindObjectOfType<AnalyticsManager>();
            _pv = GetComponent<PhotonView>();
        }

        #region Call RPC Functions
        public void RequestForMicrophone()
        {
            //Debug.Log(PhotonNetwork.LocalPlayer.ActorNumber);
            _pv.RPC(nameof(RpcRequestMicrophone), RpcTarget.Others, PhotonNetwork.LocalPlayer.ActorNumber);
            //_pv.RPC(nameof(RpcRequestMicrophone), RpcTarget.Others, AppManager.Instance.userInfo.UserData[0].id);
        }
        
        public void CloseTargetMicrophone(int targetUserId)
        {
            _pv.RPC(nameof(RpcCloseMicrophone), RpcTarget.Others, targetUserId);
        }
        
        public void AllowTargetMicrophone(int targetUserId)
        {
            _pv.RPC(nameof(RpcAllowMicrophone), RpcTarget.Others, targetUserId);
        }
        #endregion
        
        
        #region RPC Functions
        [PunRPC]
        private void RpcRequestMicrophone(int userId)
        {
            //Debug.Log("UserRole");
            if (AppManager.Instance.currentUserRole == UserRole.Moderator)
            {
                Debug.Log("UserRole.Moderator");
                ShowRequestedUi(userId);
            }
        }
        
        
        [PunRPC]
        private void RpcCloseMicrophone(int userId)
        {
            if (PhotonNetwork.LocalPlayer.ActorNumber == userId)
            {
                CloseMyMicrophone();
            }
        }
        
        [PunRPC]
        private void RpcAllowMicrophone(int userId)
        {
            if (PhotonNetwork.LocalPlayer.ActorNumber == userId)
            {
                if (_analyticsManager)
                {
                    _analyticsManager.SendUserTalkedAnalytics();
                }
                AllowMyMicrophone();
            }
        }
        #endregion

        
        #region Local Actions
        private void CloseMyMicrophone()
        {
            ReferenceManagerConference.Instance.animationController.StopTalkingAnim();
            ReferenceManagerConference.Instance.micMutedMessage.Show();
            ReferenceManagerConference.Instance.microphoneButton.SetActive(false);
            ReferenceManagerConference.Instance.voiceManager.TransmitVoiceButton(false);
        }

        private void AllowMyMicrophone()
        {
            ReferenceManagerConference.Instance.animationController.StartTalkingAnim();
            ReferenceManagerConference.Instance.requestAcceptedMessage.Show();
            ReferenceManagerConference.Instance.microphoneButton.SetActive(true);
            ReferenceManagerConference.Instance.voiceManager.TransmitVoiceButton(true);
        }
        
        private void ShowRequestedUi(int userId)
        {
            if (ReferenceManagerConference.Instance.audienceHandler.currentAudiences != null)
            {
                for (int i = 0; i < ReferenceManagerConference.Instance.audienceHandler.currentAudiences.Count; i++)
                {
                    ReferenceManagerConference.Instance.audienceHandler.currentAudiences[i].HandleIfRequestedUi(userId);
                }
            }
        }
        #endregion
    }
}
