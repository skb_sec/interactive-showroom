﻿using UnityEngine;

namespace _Project.Scripts.Conference_Scene
{
    public class FaceToCamera : MonoBehaviour
    {
        private Camera _mMainCamera;

        private void Start()
        {
            _mMainCamera = Camera.main;
        }

        void Update()
        {
            if (!_mMainCamera)
            {
                return;
            }
            
            if (gameObject.activeSelf)
            {
                transform.rotation = Quaternion.LookRotation(_mMainCamera.transform.forward);
            }
        }
    }
}
