﻿using _Project.Scripts.Core;
using Photon.Pun;
using Photon.Voice.Unity;
using UnityEngine;

namespace _Project.Scripts.Conference_Scene.Voice
{
    public class MicUserNameManager : MonoBehaviour
    {
        private PhotonView PV;
        private Recorder recorder;
        private void Start()
        {
            PV = GetComponent<PhotonView>();
            
            if (PV.IsMine)
            {
                recorder = FindObjectOfType<Recorder>();
                if (recorder)
                {
                    InvokeRepeating(nameof(CheckRecorder), 1, 0.5f);
                }
            }
        }
        
        private void OnDisable()
        {
            CancelInvoke(nameof(CheckRecorder));
        }

        
        private void CheckRecorder()
        {
            if (recorder.TransmitEnabled)
            {
                if (recorder.LevelMeter.CurrentPeakAmp > 0.05f)
                {
                    PV.RPC("RPC_SetMicUserName", RpcTarget.All, AppManager.Instance.userInfo.UserData[0].name);
                }
            }
        }

        
        [PunRPC]
        private void RPC_SetMicUserName(string micUserName)
        {
            ReferenceManagerConference.Instance.uiController.SetMicUserName(micUserName);
        }
    }
}
