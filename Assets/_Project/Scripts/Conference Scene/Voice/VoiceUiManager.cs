﻿using _Project.Scripts.Conference_Scene;
using _Project.Scripts.Multiplayer;
using Photon.Voice.PUN;
using UnityEngine;
using UnityEngine.UI;
using Recorder = Photon.Voice.Unity.Recorder;

namespace _Project.Scripts.Old.Multiplayer.Voice
{
    public class VoiceUiManager : MonoBehaviour
    {
        [SerializeField] private PlayerInstantiate _playerInstantiate;
        
        private PhotonVoiceNetwork punVoiceNetwork;
 

        public Recorder recorder;

        [SerializeField]
        private int calibrationMilliSeconds = 2000;

        private float volumeBeforeMute;

        [SerializeField] private Toggle _toggleTransmit;
        [SerializeField] private Toggle _toggleMute;
        

        private void Awake()
        {
            this.punVoiceNetwork = PhotonVoiceNetwork.Instance;
            volumeBeforeMute = AudioListener.volume;
            
            #if UNITY_IOS
                recorder.MicrophoneType = Recorder.MicType.Photon;
                recorder.UseMicrophoneTypeFallback = false;
            #endif
        }

        private void OnEnable()
        {
            _playerInstantiate.characterInstantiated += this.SetupRecorder;

            _toggleTransmit.onValueChanged.AddListener(TransmitVoiceButton);
            _toggleMute.onValueChanged.AddListener(MuteVoiceButton);
        }

        private void OnDisable()
        {
            _playerInstantiate.characterInstantiated -= this.SetupRecorder;

            _toggleTransmit.onValueChanged.RemoveListener(TransmitVoiceButton);
            _toggleMute.onValueChanged.RemoveListener(MuteVoiceButton);
        }

        private void SetupRecorder()
        {
            ReferenceManagerConference.Instance.voiceCanvas.SetActive(true);
            if (this.recorder) // probably using a global recorder
            {
                return;
            }
            
            PhotonVoiceView photonVoiceView = ReferenceManagerConference.Instance.player.GetComponent<PhotonVoiceView>();
            
            if (photonVoiceView.IsRecorder)
            {
                this.recorder = photonVoiceView.RecorderInUse;
            }
        }

        private void Start()
        {
            //CalibrateVoice();
            TransmitVoiceButton(false);
            //MuteVoiceButton(false);
        }

        private void CalibrateVoice()
        {
            if (this.recorder && !this.recorder.VoiceDetectorCalibrating)
            {
                this.recorder.VoiceDetectorCalibrate(this.calibrationMilliSeconds);
            }
        }

        public void TransmitVoiceButton(bool isTransmit)
        {
            this.recorder.TransmitEnabled = isTransmit;
            //_toggleMute.gameObject.SetActive(isTransmit);
            _toggleTransmit.isOn = isTransmit;
        }

        private void MuteVoiceButton(bool isMute)
        {
            if (isMute)
            {
                this.volumeBeforeMute = AudioListener.volume;
                AudioListener.volume = 0f;
            }
            else
            {
                AudioListener.volume = this.volumeBeforeMute;
                this.volumeBeforeMute = 0f;
            }
        }
    }
}
