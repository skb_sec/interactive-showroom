using System;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Conference_Scene.Presentation
{
    public class PresentationThumbItem : MonoBehaviour
    {
        public Image thumbImage;
        public string imageUrl;

        private Button _imageButton;

        public void Start()
        {
            ReferenceManagerConference.Instance.imageLoader.LoadImageInUi(thumbImage, imageUrl);
            
            _imageButton = GetComponent<Button>();
            _imageButton.onClick.AddListener(OnClick);
        }
        
        private string RemoveQuotation(string url)
        {
            string newRes = url;
            StringBuilder sb = new StringBuilder(newRes);
            sb.Remove(0, 1);
            sb.Remove(sb.Length-1, 1);
            newRes = sb.ToString();
        
            return newRes;
        }

        private void OnDisable()
        {
            _imageButton.onClick.RemoveListener(OnClick);
        }

        private void OnClick()
        {
            ReferenceManagerConference.Instance.presentationFullImage.sprite = thumbImage.sprite;
            ReferenceManagerConference.Instance.player.GetComponent<MultiplayerPresentationManager>().LoadSlidePresentation(imageUrl);
        }
    }
}
