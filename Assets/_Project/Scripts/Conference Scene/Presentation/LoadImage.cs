﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

namespace _Project.Scripts.Conference_Scene.Presentation
{
    public class LoadImage : MonoBehaviour
    {
        private void OnDisable()
        {
            StopAllCoroutines();
        }

        public void LoadImageInUi(Image thumbImage, string url)
        {
            StartCoroutine(GetImage(thumbImage, url));
        }

        private IEnumerator GetImage(Image thumbImage, string url)
        {
            using (var uwr = UnityWebRequestTexture.GetTexture(url))
            {
                yield return uwr.SendWebRequest();

                if (uwr.isNetworkError || uwr.isHttpError)
                {
                    Debug.Log(uwr.error);
                    yield break;
                }

                Texture2D texture = DownloadHandlerTexture.GetContent(uwr);
                uwr.Dispose();
                try
                {
                    thumbImage.sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height),
                        new Vector2(0, 0));
                }
                catch
                {
                    Debug.Log("Image might be destroyed");
                }
            }
        }
    }
}
