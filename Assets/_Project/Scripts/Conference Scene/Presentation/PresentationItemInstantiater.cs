using _Project.Scripts.Core;
using UnityEngine;

namespace _Project.Scripts.Conference_Scene.Presentation
{
    public class PresentationItemInstantiater : MonoBehaviour
    {
        public Transform allPresentationParent;
        public PresentationItem presentation;



        public void Instantiate()
        {
            DeleteAllChild.Clear(allPresentationParent);
            InstantiateAll();
        }

        private void InstantiateAll()
        {
            int count = ReferenceManagerConference.Instance.presentationDataLoader.allPresentationData.Presentations
                .Count;
            for (int i = 0; i < count; i++)
            {
                PresentationItem item = Instantiate(presentation.gameObject, allPresentationParent).GetComponent<PresentationItem>();

                item.presentationData = ReferenceManagerConference.Instance.presentationDataLoader.allPresentationData
                    .Presentations[i];
            }
        }
    }
}
