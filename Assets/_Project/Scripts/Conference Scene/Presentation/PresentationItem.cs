using System;
using _Project.Scripts.Core;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Conference_Scene.Presentation
{
    public class PresentationItem : MonoBehaviour
    {
        public TMP_Text presentationNameText;
        public TMP_Text presentationTypeText;

        public Presentation presentationData;
        public string[] allUrls;
        private Button _presentationButton;

        private void Awake()
        {
            _presentationButton = GetComponent<Button>();
        }

        private void Start()
        {
            presentationNameText.text = presentationData.name;
            presentationTypeText.text = presentationData.type;
        }

        private void OnEnable()
        {
            _presentationButton.onClick.AddListener(OnClick);
        }

        private void OnDisable()
        {
            _presentationButton.onClick.RemoveListener(OnClick);
        }

        private void OnClick()
        {
            if (presentationData.type == "Slide")
            {
                ShowSlide();
            }
            
            else if (presentationData.type == "Video")
            {
                ReferenceManagerConference.Instance.player.GetComponent<MultiplayerPresentationManager>().LoadVideoPresentation(presentationData.url);
                
                ReferenceManagerConference.Instance.fullscreenVideo.SetActive(true);
                ReferenceManagerConference.Instance.wholeCanvas.SetActive(false);
                ReferenceManagerConference.Instance.youtubePlayer.Play(presentationData.url.Replace(" ", ""));
            }
        }
        
        private void ShowSlide()
        {
            ReferenceManagerConference.Instance.singlePresentationView.Show();
            DeleteAllChild.Clear(ReferenceManagerConference.Instance.thumbParent);
            InstantiateAll();
        }
        
        private void InstantiateAll()
        {
            allUrls = presentationData.url.Split(','); 
            int count = allUrls.Length;
            
            for (int i = 0; i < count; i++)
            {
                PresentationThumbItem item = Instantiate(ReferenceManagerConference.Instance.presentationThumbItem, ReferenceManagerConference.Instance.thumbParent).GetComponent<PresentationThumbItem>();

                item.imageUrl = allUrls[i].Replace(" ", "");;
                Debug.Log(item.imageUrl);
            }
        }
    }
}
