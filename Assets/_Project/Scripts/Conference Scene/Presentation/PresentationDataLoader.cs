using System.Collections;
using _Project.Scripts.Core;
using UnityEngine;
using UnityEngine.Networking;

namespace _Project.Scripts.Conference_Scene.Presentation
{
    public class PresentationDataLoader : MonoBehaviour
    {
        public PresentationDataResponse allPresentationData;
        
        private void Start()
        {
            StartCoroutine(LoadPresentationData());
        }

        private IEnumerator LoadPresentationData()
        {
            using (var uwr = UnityWebRequest.Get(AllUrls.AllPresentationUrl))
            {
                yield return uwr.SendWebRequest();

                if (uwr.isNetworkError || uwr.isHttpError)
                {
#if UNITY_EDITOR
                    Debug.Log(uwr.error);
                    Debug.Log(uwr.downloadHandler.text);
#endif
                }

                else
                {
#if UNITY_EDITOR
                    Debug.Log(uwr.downloadHandler.text);
#endif
                    allPresentationData = JsonUtility.FromJson<PresentationDataResponse>(uwr.downloadHandler.text);
                }
            }
        }
    }
}
