using System;
using System.Collections.Generic;

namespace _Project.Scripts.Conference_Scene.Presentation
{
    // [Serializable]
    // public class Presentation
    // {
    //     public int id;
    //     public string name;
    //     public string type;
    //     public string url;
    // }
    //
    // [Serializable]
    // public class PresentationDataResponse
    // {
    //     public List<Presentation> allPresentations;
    // }
    
    [Serializable]
    public class Presentation
    {
        public int id;
        public string name;
        public string type;
        public string url;
    }
    [Serializable]
    public class PresentationDataResponse
    {
        public List<Presentation> Presentations;
    }
}