using System;
using System.Collections.Generic;
using _Project.Scripts.Core;
using UnityEngine;
using UnityEngine.EventSystems;

namespace _Project.Scripts.Conference_Scene
{
    public class AvatarPositioner : MonoBehaviour
    {
        public Transform speakerInitialPos;
        public Transform moderatorPos;
        
        public Transform allSeatParent;
        public List<AvatarPositions> audiencePos = new List<AvatarPositions>();
        
        public AudienceAvatar[] avatars;

        private Camera camera;
        Vector3 positionOffset = new Vector3(0, 0.5f, 0);

        private int selectedChairIndex = -1;
        
        private void Start()
        {
            camera = Camera.main;
            AssignSeatIndex();
        }

        private void AssignSeatIndex()
        {
            for (int i = 0; i < allSeatParent.childCount; i++)
            {
                AvatarPositions audPos = new AvatarPositions();
                audPos.posTransform = allSeatParent.GetChild(i);
                audPos.unavailabe = false;

                audiencePos.Add(audPos);
            }
        }

        private void Update()
        {
            if (ReferenceManagerConference.Instance == null || !ReferenceManagerConference.Instance.changeSeatButton.isOn)
            {
                return;
            }
            
            if (EventSystem.current.currentSelectedGameObject != null)
            {
                return;
            }
            
            if (AppManager.Instance.currentUserRole == UserRole.Audience)
            {
                // Mouse Click
                if (Input.GetMouseButtonDown(0))
                {
                    HandleInputBegin();
                }
                
                else if (Input.GetMouseButtonUp(0))
                {
                    HandleInputUp();
                    HidePlacementMarker();
                    selectedChairIndex = -1;
                }

                // Touch
                if (Input.touchCount > 0)
                {
                    Touch touch = Input.GetTouch(0);
                    
                    if (touch.phase == TouchPhase.Began)
                    {
                        HandleInputBegin();
                    }
                    else if (touch.phase == TouchPhase.Moved)
                    {
                        HidePlacementMarker();
                        selectedChairIndex = -1;
                    }
                    else if (touch.phase == TouchPhase.Ended)
                    {
                        HandleInputUp();
                        HidePlacementMarker();
                        selectedChairIndex = -1;
                    }
                }
                
            }
        }

        private void HandleInputBegin()
        {
            RaycastHit hit;
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);
                
            if (Physics.Raycast(ray, out hit)) {
                if (hit.collider.CompareTag($"chair"))
                {
                    Chair chair = hit.collider.gameObject.GetComponent<Chair>();
                    selectedChairIndex = chair.seatNo;
                            
                    if (!audiencePos[chair.seatNo].unavailabe)
                    {
                        ShowPlacementMarker(hit.collider.gameObject.transform);
                    }
                    else
                    {
                        Debug.Log("Seat has been taken");
                    }
                }
            }
        }

        private void HandleInputUp()
        {
            RaycastHit hit;
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);
                
            if (Physics.Raycast(ray, out hit)) {
                if (hit.collider.CompareTag($"chair"))
                {
                    //Debug.Log("hit on chair");
                    Chair chair = hit.collider.gameObject.GetComponent<Chair>();

                    if (chair.seatNo == selectedChairIndex)
                    {
                        if (!audiencePos[chair.seatNo].unavailabe)
                        {
                            RepositionAvatar(hit.collider.gameObject.transform, chair.seatNo);
                        }
                        else
                        {
                            Debug.Log("Seat has been taken");
                        }
                    }
                }
            }
        }

        public PositionRotation PlayerInstantiatePositionRotation()
        {
            PositionRotation positionRotation = new PositionRotation();
            
            switch (AppManager.Instance.currentUserRole)
            {
                case UserRole.Speaker:
                    positionRotation.positionVector = speakerInitialPos.position;
                    positionRotation.rotationVector = Quaternion.Euler(0f, 180f, 0f);
                    break;
                
                case UserRole.Moderator:
                    positionRotation.positionVector = moderatorPos.position;
                    positionRotation.rotationVector = positionRotation.rotationVector = Quaternion.identity;
                    break;
                
                default:
                    positionRotation.positionVector = audiencePos[0].posTransform.position - positionOffset;
                    positionRotation.rotationVector = Quaternion.identity;
                    break;
            }

            return positionRotation;
        }

        public int GetAvailablePos()
        {
            for (int i = 0; i < audiencePos.Count; i++)
            {
                if (!audiencePos[i].unavailabe)
                {
                    return i;
                }
            }

            return -1;
        }

        private void ShowPlacementMarker(Transform parent)
        {
            ReferenceManagerConference.Instance.placementMarker.transform.SetParent(parent);
            ReferenceManagerConference.Instance.placementMarker.transform.position = Vector3.zero;
            ReferenceManagerConference.Instance.placementMarker.SetActive(true);
            
        }

        public void RepositionAvatar(Transform parent, int newSeatNo)
        {
            ReferenceManagerConference.Instance.player.transform.localPosition = parent.position - positionOffset;
            ReferenceManagerConference.Instance.player.GetComponent<AudienceAvatar>().UpdateMySeat(newSeatNo);
            ReferenceManagerConference.Instance.emojiManager.PositionEmojiCanvas();
        }

        private void HidePlacementMarker()
        {
            ReferenceManagerConference.Instance.placementMarker.SetActive(false);
        }
        
        public void UpdatePosList()
        {
            Invoke(nameof(UpdateList), 1.0f);
        }

        private void UpdateList()
        {
            avatars = FindObjectsOfType<AudienceAvatar>();

            ClearAllSeat();
            
            for (int i = 0; i < avatars.Length; i++)
            {
                int seatIndex = avatars[i].currentSeatNo;
                
                if (seatIndex >= 0)
                {
                    audiencePos[avatars[i].currentSeatNo].unavailabe = true;
                }
            }
        }

        private void ClearAllSeat()
        {
            for (int i = 0; i < audiencePos.Count; i++)
            {
                audiencePos[i].unavailabe = false;
            }
        }
    }

    public struct PositionRotation
    {
        public int seatNo;
        public Vector3 positionVector;
        public Quaternion rotationVector;
    }

    [Serializable]
    public class AvatarPositions
    {
        public Transform posTransform;
        public bool unavailabe;
    }
}
