using Paroxe.PdfRenderer;
using UnityEngine;

namespace _Project.Scripts.Conference_Scene
{
    public class PdfController : MonoBehaviour
    {
        public PDFViewer pdfViewerWorld;
        public PDFViewer pdfViewerScreen;
        
        public void LoadDocumentFromLink(string link)
        {
            pdfViewerScreen.PageFitting = PDFViewer.PageFittingType.ViewerWidth;
            pdfViewerScreen.LoadDocumentFromWeb(link);
        }
        
        public void ClosePresentation()
        {
            Destroy(gameObject);
        }
        
        public void LoadDocumentInWorldFromLink(string link)
        {
            pdfViewerWorld.PageFitting = PDFViewer.PageFittingType.ViewerWidth;
            pdfViewerWorld.LoadDocumentFromWeb(link);
        }
    }
}
