using UnityEngine;

namespace _Project.Scripts.Conference_Scene
{
    public class EmojiManager : MonoBehaviour
    {
        private int count;
        private int maxEmoji = 7;
        private Sprite selectedEmojiSprite;
        
        public Vector3 emojiCanvasOffset;
        public float speed;

        public void PositionEmojiCanvas()
        {
            ReferenceManagerConference.Instance.emojiCanvas.transform.position =
                ReferenceManagerConference.Instance.player.transform.position + emojiCanvasOffset;
        }
        
        public void OnClickThumbsUp()
        {
            selectedEmojiSprite = ReferenceManagerConference.Instance.thumbsUpEmojiSprite;
            InvokeInstantiate();
        }
        
        public void OnClickClap()
        {
            selectedEmojiSprite = ReferenceManagerConference.Instance.clapEmojiSprite;
            InvokeInstantiate();
        }
        
        public void OnClickDisbelief()
        {
            selectedEmojiSprite = ReferenceManagerConference.Instance.disbeliefEmojiSprite;
            InvokeInstantiate();
        }
        
        public void OnClickQuestion()
        {
            selectedEmojiSprite = ReferenceManagerConference.Instance.questionEmojiSprite;
            InvokeInstantiate();
        }

        private void InvokeInstantiate()
        {
            CancelInvoke(nameof(InstantiateEmoji));
            count = 0;
            InvokeRepeating(nameof(InstantiateEmoji), 0, 0.5f);
        }

        private void InstantiateEmoji()
        {
            EmojiMotion emoji = Instantiate(ReferenceManagerConference.Instance.emojiPrefab,
                ReferenceManagerConference.Instance.emojiParent);
            emoji.speed = speed;
            emoji.emoji.sprite = selectedEmojiSprite;
            emoji.transform.localPosition = Vector3.zero + new Vector3(Random.Range(-0.1f, 0.1f), 0, 0);
            count += 1;

            if (count >= maxEmoji)
            {
                CancelInvoke(nameof(InstantiateEmoji));
            }
        }
    }
}
