using _Project.Scripts.Conference_Scene.Chat;
using _Project.Scripts.Conference_Scene.Presentation;
using _Project.Scripts.Old.Multiplayer.Voice;
using Doozy.Engine.UI;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Conference_Scene
{
    public class ReferenceManagerConference : MonoBehaviour
    {
        public GameObject player;
        
        [Header("General UI Reference")] 
        public GameObject controllerCanvas;
        public GameObject interactionPanel;
        public GameObject voiceCanvas;
        public GameObject placementMarker;
        public UIPopup disconnectPopup;
        public UIPopup sameUserExistPopup;
        public GameObject generalCanvas;

        [Header("Scripts Reference")] 
        public ChatGui chatManager;
        public VoiceUiManager voiceManager;
        public PlayerInstantiate playerInstantiate;
        public AnimationController animationController;
        public AvatarPositioner avatarPositioner;
        public SpeakerPositionManager speakerPositioner;
        public UiControllerConference uiController;
        public EmojiManager emojiManager;
        public PdfController pdfController;
        public LoadImage imageLoader;

        [Header("Message View Reference")] 
        public UIView requestSentMessage;
        public UIView requestAcceptedMessage;
        public UIView micMutedMessage;

        [Header("Role Management Reference")]
        public GameObject presentationCanvasWorld;
        public GameObject presentationCanvasScreen;
        public GameObject optionsCanvas;
        public GameObject requestToTalkButton;
        public GameObject allAudiencePanel;
        public GameObject audiencePrefab;
        public Transform allAudienceParent;
        public TMP_Text allAudienceCount;
        public Transform requestingAudienceParent;
        public TMP_Text requestingAudienceCount;
        public GameObject allAudienceListPanel;
        public GameObject requestingMicAudienceListPanel;
        public GameObject allAudienceButton;
        public GameObject microphoneButton;
        public Toggle changeSeatButton;
        public GameObject conferenceMaterials;
        public Toggle speakerStandingPos;
        public GameObject saveChatLogButton;
        public AudienceHandler audienceHandler;


        [Header("Presentation Reference")] 
        public GameObject presentationCamera;
        public Image presentationFullImage;
        public Transform thumbParent;
        public PresentationThumbItem presentationThumbItem;
        public PresentationDataLoader presentationDataLoader;
        public UIView singlePresentationView;
        public GameObject presentation;
        public Image worldSlideImage;
        public YoutubeSimplified videoObject;
        public YoutubePlayer youtubePlayer;
        public GameObject fullscreenVideo;
        public GameObject slideObject;
        public GameObject wholeCanvas;

        [Header("Emoji Reference")] 
        public GameObject emojiCanvas;
        public EmojiMotion emojiPrefab;
        public Transform emojiParent;
        public Sprite clapEmojiSprite;
        public Sprite thumbsUpEmojiSprite;
        public Sprite disbeliefEmojiSprite;
        public Sprite questionEmojiSprite;

        [Header("Chat Reference")]
        public Button chatButton;
        public Button sendButton;

        //public string chatUsername;
        //public string destinationUsername;
        
        public TMP_Text micUsernameText;
        public GameObject chatNotification;
        public GameObject chatCanvas;
        
        public AvatarAppearanceHandler[] allAvatar;

        public string presentationName;
        public Transform presentationParent;
    
        public static ReferenceManagerConference Instance;
        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
        }
    }
}
