using System;
using UnityEngine;

namespace _Project.Scripts.Conference_Scene
{
    public class Chair : MonoBehaviour
    {
        public int seatNo;

        private void Awake()
        {
            seatNo = transform.GetSiblingIndex();
        }
    }
}
